package com.example.SpringBootRestApi.util;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class Mapper {

  public ModelMapper mapper(){
    return new ModelMapper();
  }
}
