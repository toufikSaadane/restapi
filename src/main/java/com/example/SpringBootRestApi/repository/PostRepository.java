package com.example.SpringBootRestApi.repository;

import com.example.SpringBootRestApi.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PostRepository extends JpaRepository<Post,Long> {

}
