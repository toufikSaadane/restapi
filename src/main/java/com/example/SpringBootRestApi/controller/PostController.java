package com.example.SpringBootRestApi.controller;

import com.example.SpringBootRestApi.entity.Dto.PostDto;
import com.example.SpringBootRestApi.entity.ResponseFormat.PostResponse;
import com.example.SpringBootRestApi.service.serviceImplementations.PostServiceImplementation;
import com.example.SpringBootRestApi.util.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/posts")
public class PostController {

  private final PostServiceImplementation postServiceInmplementation;

  @Autowired
  public PostController(PostServiceImplementation postServiceInmplementation) {
    PostServiceImplementation p = postServiceInmplementation;
    this.postServiceInmplementation = postServiceInmplementation;
  }

  @PostMapping
  public ResponseEntity<PostDto> createPost(@Valid @RequestBody PostDto postDto) {
    return new ResponseEntity<>(postServiceInmplementation.createPost(postDto), HttpStatus.CREATED);
  }

  @GetMapping
  public ResponseEntity<PostResponse> getPosts(
          @RequestParam(value = "pageNumber", defaultValue= AppConstants.DEFAULT_PAGE_NUMBER,required = false) int pageNumber,
          @RequestParam(value = "pageSize",  defaultValue= AppConstants.DEFAULT_PAGE_SIZE ,required = false) int pageSize,
          @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_PAGE_SORT_PARAM, required = false) String sortBy
  ) {
    return new ResponseEntity<>(postServiceInmplementation.getAllPosts(pageNumber, pageSize, sortBy), HttpStatus.OK);
  }

  @GetMapping(value = "{postId}")
  public ResponseEntity<PostDto> findPostById(@PathVariable( name = "postId") Long id) {
    return new ResponseEntity<>(postServiceInmplementation.getPostById(id), HttpStatus.CREATED);
  }

  @PutMapping(value = "{postId}/update")
  public ResponseEntity<PostDto> updatePost(@Valid @RequestBody PostDto postDto, @PathVariable(name = "postId") Long id){
    return new ResponseEntity<>(postServiceInmplementation.updatePostById(postDto, id), HttpStatus.OK);
  }

  @DeleteMapping(value = "{postId}/delete")
  public  ResponseEntity<String> deletePostById(@PathVariable(name = "postId") Long id){
    postServiceInmplementation.deletePost(id);
    String message = String.format("Post %d deleted", id);
    return new ResponseEntity<>(message, HttpStatus.OK);
  }
}
